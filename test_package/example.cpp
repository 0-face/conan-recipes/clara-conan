#include "clara.hpp"

#include <iostream>

using namespace std;
using namespace clara;

int main(){
    std::cout << "*************** Clara example ********************" << std::endl;

    int width = 0;
    std::string name;
    bool doIt = false, showHelp = false;
    std::string command;
    auto cli
        = Help(showHelp)
        | Opt( width, "width" )
            ["-w"]["--width"]
            ("How wide should it be?")
        | Opt( name, "name" )
            ["-n"]["--name"]
            ("By what name should I be known")
        | Opt( doIt )
            ["-d"]["--doit"]
            ("Do the thing" )
        | Arg( command, "command" )
            ("which command to run");

    const char * argv[] = {"program_name", "--help"};
    auto result = cli.parse( Args( 2, argv ) );
    if( !result ) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
//        exit(1);
    }
    else if (showHelp) {
        std::cout << cli << std::endl;
    }

    std::cout << "****************************************************" << std::endl;

    return 0;
}
