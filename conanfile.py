import os

from conans import ConanFile, tools


class Recipe(ConanFile):
    name        = "clara"
    version     = "1.1.4"
    license     = "BSL 1.0 (Boost Software License 1.0)"
    description = "A simple to use, composable, command line parser for C++ 11 and beyond"
    no_copy_source = True

    repo        = "https://github.com/catchorg/Clara"
    url         = "https://gitlab.com/0-face/conan-recipes/clara-conan"

    release_sha256 = '442e9a90879b2724b9ab4657184111c694c6cca792bb1d2047568cc07f53560a'

    def source(self):
        header     = "clara.hpp"
        header_url = "{}/releases/download/v{}/{}".format(self.repo, self.version, header)

        tools.download(header_url, header)
        tools.check_sha256(header, self.release_sha256)

    def package(self):
        self.copy("clara.hpp", dst="include")
